// Fill out your copyright notice in the Description page of Project Settings.


#include "Cauldron.h"

#include "Bucket.h"
#include "GGJ23Character.h"
#include "Tool.h"


// Sets default values
ACauldron::ACauldron()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ACauldron::OnFinished()
{
	m_bucket->m_isFull = true;
}

// Called when the game starts or when spawned
void ACauldron::BeginPlay()
{
	Super::BeginPlay();

	auto* sm = CastChecked<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	sm->OnComponentHit.AddDynamic(this, &ACauldron::OnHit);

	m_onFinished.AddDynamic(this, &ACauldron::OnFinished);

	m_bucket->m_mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ACauldron::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	if(auto * player = CastChecked<AGGJ23Character>(OtherActor))
	{
		if (player->m_numberOfRoots == 0) return;
		m_numberOfRoots += player->m_numberOfRoots;
		m_numberOfRootsToUse += player->m_numberOfRoots;
		player->m_numberOfRoots = 0;
		player->m_onDeposited.Broadcast();

		m_onReceiveRoots.Broadcast();

		if(!GetWorldTimerManager().IsTimerActive(m_timerBrew))
		{
			if(m_numberOfRootsToUse >= m_numberOfRootsByJuice)
			{
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Starting brewing");
				m_numberOfRootsToUse -= m_numberOfRootsByJuice;
				GetWorldTimerManager().SetTimer(m_timerBrew, this, &ACauldron::OnBrewed, m_secondsToBrewJuice);
			}
		}
	}
}

void ACauldron::OnBrewed()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Brewed");
	m_onBrewed.Broadcast();

	m_numberOfJuice++;

	if(m_numberOfJuice >= m_numberOfJuiceToFinish)
	{
		m_onFinished.Broadcast();
	}
}

// Called every frame
void ACauldron::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

