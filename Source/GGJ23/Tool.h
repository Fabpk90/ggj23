// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "Tool.generated.h"

class ARoot;
class AGGJ23Character;
struct FInputBindingHandle;
UCLASS()
class GGJ23_API ATool : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATool();
	
	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputMappingContext* m_mappingContext;

	/** Jump Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputAction* m_useInputAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	class UInputAction* m_throwInputAction;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent * m_mesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USphereComponent * m_collisionSphere;
	
	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ARoot * m_rootNear;

	UFUNCTION()
	virtual bool CanInteractWithRoot(ARoot * _root);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	virtual void UseAction();
	virtual void ThrowAction();
public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void PickUpTool();
	
	void UnPickUpTool();
};
