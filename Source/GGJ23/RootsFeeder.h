// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tool.h"
#include "GameFramework/Actor.h"
#include "RootsFeeder.generated.h"

class AGGJ23Character;
UCLASS()
class GGJ23_API ARootsFeeder : public ATool
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARootsFeeder();
	
	virtual void UseAction() override;
	virtual bool CanInteractWithRoot(ARoot* _root) override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
