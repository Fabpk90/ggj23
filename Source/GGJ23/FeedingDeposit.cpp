// Fill out your copyright notice in the Description page of Project Settings.


#include "FeedingDeposit.h"

#include "RootsFeeder.h"


// Sets default values
AFeedingDeposit::AFeedingDeposit()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AFeedingDeposit::SpawnFood()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Spawning food");

	for(int i = 0; i < m_numberOfFoodPerRound; ++i)
	{
		const auto pos = GetActorLocation() + FVector3d(FMath::RandPointInCircle(500.0f), 0.0f);
		const auto rot = FRotator::ZeroRotator;
		FActorSpawnParameters params;
		params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
		GetWorld()->SpawnActor<ARootsFeeder>(m_feederBP, pos, rot, params);	
	}
}

// Called when the game starts or when spawned
void AFeedingDeposit::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(m_timerSpawnMate, this, &AFeedingDeposit::SpawnFood, m_roundDurationInSeconds, true);
}

// Called every frame
void AFeedingDeposit::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

