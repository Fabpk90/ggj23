// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tool.h"
#include "GameFramework/Actor.h"
#include "Bucket.generated.h"

UCLASS()
class GGJ23_API ABucket : public ATool
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ABucket();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	virtual bool CanInteractWithRoot(ARoot* _root) override;

	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool m_isFull = false;
};
