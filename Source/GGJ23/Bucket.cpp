// Fill out your copyright notice in the Description page of Project Settings.


#include "Bucket.h"

#include "GGJ23Character.h"


// Sets default values
ABucket::ABucket()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void ABucket::BeginPlay()
{
	Super::BeginPlay();
}

bool ABucket::CanInteractWithRoot(ARoot* _root)
{
	return false;
}

void ABucket::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Entering something");
	if(auto* player = Cast<AGGJ23Character>(OtherActor))
	{
		if(m_isFull)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Entering player");
			m_mesh->SetRenderCustomDepth(true);
			m_mesh->SetCustomDepthStencilValue(1);

			player->ToolNear(this);	
		}
	}
}

// Called every frame
void ABucket::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

