// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Cauldron.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnFinished)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnReceiveRoots)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBrewed)

UCLASS()
class GGJ23_API ACauldron : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACauldron();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int m_numberOfJuiceToFinish = 0;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(ToolTip="Utile pour faire un high score surtout"))
	int m_numberOfRoots = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int m_numberOfRootsToUse = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int m_numberOfRootsByJuice = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float m_secondsToBrewJuice = 2;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	int m_numberOfJuice = 0;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FOnReceiveRoots m_onReceiveRoots;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FOnFinished m_onFinished;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FOnBrewed m_onBrewed;

	UFUNCTION()
	void OnHit( UPrimitiveComponent* HitComponent, AActor* OtherActor, class UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit );

	UFUNCTION()
	void OnBrewed();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class ABucket * m_bucket;

	UFUNCTION()
	void OnFinished();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY()
	FTimerHandle m_timerBrew;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
