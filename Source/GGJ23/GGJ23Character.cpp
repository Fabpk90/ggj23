// Copyright Epic Games, Inc. All Rights Reserved.

#include "GGJ23Character.h"

#include "Root.h"
#include "Tool.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"

AGGJ23Character::AGGJ23Character()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AGGJ23Character::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
}

void AGGJ23Character::BeginPlay()
{
	Super::BeginPlay();

	m_onPickedUp.AddDynamic(this, &AGGJ23Character::OnPickUp);

	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AGGJ23Character::OnOverlapBegin);
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &AGGJ23Character::AGGJ23Character::OnOverlapEnd);
}

void AGGJ23Character::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Overlapping something with character");
	if(!m_toolInHand) return;;
	if(auto* root = Cast<ARoot>(OtherActor))
	{
		if(m_toolInHand->CanInteractWithRoot(root))
		{
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "it's a root !");
			USkeletalMeshComponent* sm = CastChecked<USkeletalMeshComponent>(root->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
			sm->SetRenderCustomDepth(true);
			sm->SetCustomDepthStencilValue(1);
			m_toolInHand->m_rootNear = root;
		}
	}
}

void AGGJ23Character::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if(!m_toolInHand) return;
	if(auto* root = Cast<ARoot>(OtherActor))
	{
		USkeletalMeshComponent* sm = CastChecked<USkeletalMeshComponent>(root->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		sm->SetRenderCustomDepth(false);
		m_toolInHand->m_rootNear = nullptr;
	}
}

void AGGJ23Character::OnPickUp()
{
	m_numberOfRoots++;
}

void AGGJ23Character::ToolNear(ATool* tool)
{
	m_toolNear = tool;
}

void AGGJ23Character::ToolTooFarAway()
{
	m_toolNear = nullptr;
}

void AGGJ23Character::ToolPickUp()
{
	if(m_toolNear)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "tool near picked up !");

		m_toolNear->AttachToActor(this, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true));
		m_toolNear->PickUpTool();
		m_toolInHand = m_toolNear;
		m_toolNear = nullptr;
	}
}

void AGGJ23Character::ToolUnPickUp()
{
	m_toolInHand = nullptr;
}
