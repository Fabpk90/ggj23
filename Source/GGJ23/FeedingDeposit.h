// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Actor.h"
#include "FeedingDeposit.generated.h"

class ARootsFeeder;
UCLASS()
class GGJ23_API AFeedingDeposit : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFeedingDeposit();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int m_numberOfFood;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int m_numberOfFoodPerRound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float m_roundDurationInSeconds;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ARootsFeeder> m_feederBP;

	UFUNCTION()
	void SpawnFood();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
	FTimerHandle m_timerSpawnMate;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
