// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Root.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnReadyToMate)

class UBlackboardComponent;
UCLASS()
class GGJ23_API ARoot : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARoot();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int m_food = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int m_foodNeededToMate = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ARoot> m_rootToSpawn;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBlackboardComponent * m_blackboard;

	UFUNCTION(BlueprintNativeEvent)
	void Feed(int _amount);

	UFUNCTION()
	bool CanMate();

	UFUNCTION()
	bool CanBeCut();

	UFUNCTION(BlueprintNativeEvent)
	void Cut();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AActor> m_rootsCut;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int m_numberOfRootsWhenCut = 1;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void Mated();

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FOnReadyToMate OnReadyToMate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool m_canMate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool m_canBeCut;
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
