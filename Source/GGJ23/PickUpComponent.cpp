// Fill out your copyright notice in the Description page of Project Settings.


#include "PickUpComponent.h"

#include "GGJ23Character.h"


// Sets default values for this component's properties
UPickUpComponent::UPickUpComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPickUpComponent::BeginPlay()
{
	Super::BeginPlay();

	m_mesh = CastChecked<UStaticMeshComponent>(GetOwner()->GetComponentByClass(UStaticMeshComponent::StaticClass()));
	m_mesh->OnComponentHit.AddDynamic(this, &UPickUpComponent::OnHit);
	
}


void UPickUpComponent::OnHit( UPrimitiveComponent* HitComponent,
	AActor* OtherActor,  UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Touched something");

	if(auto * player = Cast<AGGJ23Character>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "I'm being picked up !");

		player->m_onPickedUp.Broadcast();
		GetOwner()->Destroy();
	}
}

// Called every frame
void UPickUpComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                     FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

