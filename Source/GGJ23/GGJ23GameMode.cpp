// Copyright Epic Games, Inc. All Rights Reserved.

#include "GGJ23GameMode.h"
#include "GGJ23PlayerController.h"
#include "GGJ23Character.h"
#include "UObject/ConstructorHelpers.h"

AGGJ23GameMode::AGGJ23GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AGGJ23PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}