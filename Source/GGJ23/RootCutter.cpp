// Fill out your copyright notice in the Description page of Project Settings.


#include "RootCutter.h"

#include "GGJ23Character.h"
#include "Root.h"


// Sets default values
ARootCutter::ARootCutter()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

bool ARootCutter::CanInteractWithRoot(ARoot* _root)
{
	return _root->CanBeCut();
}

void ARootCutter::UseAction()
{
	if(m_rootNear && m_rootNear->CanBeCut())
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Using tool");
		CastChecked<AGGJ23Character>(GetWorld()->GetFirstPlayerController()->GetCharacter())->m_isCutting = true;
		m_rootNear->Cut();
		m_onCut.Broadcast();
	}
}

// Called when the game starts or when spawned
void ARootCutter::BeginPlay()
{
	Super::BeginPlay();
	
}

