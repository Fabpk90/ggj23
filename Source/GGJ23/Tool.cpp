// Fill out your copyright notice in the Description page of Project Settings.


#include "Tool.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "GGJ23Character.h"
#include "Components/SphereComponent.h"


// Sets default values
ATool::ATool()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	m_mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	m_mesh->SetupAttachment(RootComponent);

	m_collisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	m_collisionSphere->SetupAttachment(m_mesh);
}

void ATool::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Entering something");
	if(auto* player = Cast<AGGJ23Character>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Entering player");
		m_mesh->SetRenderCustomDepth(true);
		m_mesh->SetCustomDepthStencilValue(1);

		player->ToolNear(this);
	}
}

void ATool::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                         AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Leaving something");
	if(auto* player = Cast<AGGJ23Character>(OtherActor))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Leaving player");
		m_mesh->SetRenderCustomDepth(false);
		player->ToolTooFarAway();
	}
}

bool ATool::CanInteractWithRoot(ARoot* _root)
{
	return true;
}

// Called when the game starts or when spawned
void ATool::BeginPlay()
{
	Super::BeginPlay();

	m_collisionSphere->OnComponentBeginOverlap.AddDynamic(this, &ATool::OnOverlapBegin);
	m_collisionSphere->OnComponentEndOverlap.AddDynamic(this, &ATool::OnOverlapEnd);

	m_mesh->SetSimulatePhysics(true);
}

void ATool::UseAction()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "test000");
}

void ATool::ThrowAction()
{
	m_mesh->RemoveFromRoot();
	m_mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	m_mesh->SetSimulatePhysics(true);
	UnPickUpTool();
	m_mesh->AddImpulse(GetWorld()->GetFirstPlayerController()->GetCharacter()->GetActorForwardVector() * 1000.0f);
	CastChecked<AGGJ23Character>(GetWorld()->GetFirstPlayerController()->GetCharacter())->m_isThrowing = true;
}

// Called every frame
void ATool::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ATool::PickUpTool()
{
	if(m_mappingContext)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Setting up tool bindings");
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetWorld()->GetFirstLocalPlayerFromController()))
		{
			Subsystem->AddMappingContext(m_mappingContext, 1);
		}

		UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(GetWorld()->GetFirstPlayerController()->InputComponent);

		EnhancedInputComponent->BindAction(m_useInputAction, ETriggerEvent::Triggered,this, &ATool::UseAction);
		EnhancedInputComponent->BindAction(m_throwInputAction, ETriggerEvent::Triggered,this, &ATool::ThrowAction);
	}
	
	m_collisionSphere->Deactivate();
	m_mesh->SetSimulatePhysics(false);
	m_mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ATool::UnPickUpTool()
{
	if(m_mappingContext)
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetWorld()->GetFirstLocalPlayerFromController()))
		{
			Subsystem->RemoveMappingContext(m_mappingContext);
		}

		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Removing inputs of tool");
	}
	
	Cast<AGGJ23Character>(GetWorld()->GetFirstPlayerController()->GetCharacter())->ToolUnPickUp();	
	UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(GetWorld()->GetFirstPlayerController()->InputComponent);
	//check(EnhancedInputComponent->Remove;
	m_collisionSphere->Activate();
	m_mesh->SetSimulatePhysics(true);
}