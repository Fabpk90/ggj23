// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Tool.h"
#include "GameFramework/Actor.h"
#include "RootCutter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCut)

UCLASS()
class GGJ23_API ARootCutter : public ATool
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ARootCutter();
	
	virtual bool CanInteractWithRoot(ARoot* _root) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FOnCut m_onCut;

protected:
	virtual void UseAction() override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
};
