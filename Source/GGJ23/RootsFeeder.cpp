// Fill out your copyright notice in the Description page of Project Settings.


#include "RootsFeeder.h"

#include "GGJ23Character.h"
#include "Root.h"


// Sets default values
ARootsFeeder::ARootsFeeder()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ARootsFeeder::UseAction()
{
	Super::UseAction();
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Using tool");

	if(m_rootNear && !m_rootNear->CanMate())
	{
		m_rootNear->Feed(1);
		//todo delete this after use
		Destroy();

		CastChecked<AGGJ23Character>(GetWorld()->GetFirstPlayerController()->GetCharacter())->m_toolInHand = nullptr;
		USkeletalMeshComponent* sm = CastChecked<USkeletalMeshComponent>(m_rootNear->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		sm->SetRenderCustomDepth(false);
	}
}

bool ARootsFeeder::CanInteractWithRoot(ARoot* _root)
{
	return !_root->CanMate();
}

// Called when the game starts or when spawned
void ARootsFeeder::BeginPlay()
{
	Super::BeginPlay();
}

