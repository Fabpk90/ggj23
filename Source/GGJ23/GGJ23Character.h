// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GGJ23Character.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnPickedUp)
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDesposited)

UCLASS(Blueprintable)
class AGGJ23Character : public ACharacter
{
	GENERATED_BODY()

public:
	AGGJ23Character();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void BeginPlay() override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int m_numberOfRoots = 0;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FOnPickedUp m_onPickedUp;

	UPROPERTY(EditAnywhere, BlueprintAssignable)
	FOnDesposited m_onDeposited;

	UFUNCTION()
	void OnPickUp();

	UFUNCTION()
	void ToolNear(class ATool * tool);

	UFUNCTION()
	void ToolTooFarAway();

	UFUNCTION()
	void ToolPickUp();

	UFUNCTION()
	void ToolUnPickUp();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class ATool * m_toolInHand;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool m_isCutting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool m_isThrowing;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere)
	class ATool * m_toolNear;
};

