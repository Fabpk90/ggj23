// Fill out your copyright notice in the Description page of Project Settings.


#include "Root.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"


// Sets default values
ARoot::ARoot()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ARoot::Feed_Implementation(int _amount)
{
	m_food += _amount;

	if(m_food >= m_foodNeededToMate)
	{
		m_canMate = true;

		m_blackboard->SetValueAsBool("CanMate", true);
		OnReadyToMate.Broadcast();
	}
}

bool ARoot::CanMate()
{
	return m_canMate;
}

bool ARoot::CanBeCut()
{
	return m_canBeCut;
}

void ARoot::Cut_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "Cutting root");

	for(int i = 0; i < m_numberOfRootsWhenCut; ++i)
	{
		const auto pos = GetActorLocation() + FVector3d(FMath::RandPointInCircle(200.0f), 0.0f);
		const auto rot = FRotator::ZeroRotator;
		FActorSpawnParameters params;
		params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
		GetWorld()->SpawnActor<AActor>(m_rootsCut, pos, rot, params);	
	}

	Destroy();
}

void ARoot::Mated_Implementation()
{
	m_canBeCut = true;
}

// Called when the game starts or when spawned
void ARoot::BeginPlay()
{
	Super::BeginPlay();

	m_blackboard = UAIBlueprintHelperLibrary::GetBlackboard(this);
}

// Called every frame
void ARoot::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}